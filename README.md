# 腾讯云短视频终端组件 UGSV SDK

## SDK 下载

访问 Github 较慢的客户可以考虑使用国内下载地址：  [DOWNLOAD](https://cloud.tencent.com/document/product/584/9366) 。

| 所属平台 | Zip下载                                                      | SDK集成指引                                                 |
| -------- | ------------------------------------------------------------ | ----------------------------------------------------------- |
| iOS      | [下载](http://liteavsdk-1252463788.cosgz.myqcloud.com/TXLiteAVSDK_UGC_iOS_latest.zip) | [DOC](https://cloud.tencent.com/document/product/584/11638) |
| Android  | [下载](http://liteavsdk-1252463788.cosgz.myqcloud.com/TXLiteAVSDK_UGC_Android_latest.zip) | [DOC](https://cloud.tencent.com/document/product/584/11631) |

### Version 8.5 @ 2021.03.18
- iOS & Android：高级美颜效果优化，优化瘦脸、大眼、V脸等相关效果；
- iOS & Android：高级美颜新增窄脸接口；
- iOS & Android：高级美颜人脸特征提取优化；
- iOS & Android：高级美颜新增窄脸接口；
- iOS & Android：优化超级播放器播放部分网络串流seek慢的问题；
- Android: 修复超级播放器通过fileid方式播放出现报错问题；

## 问题反馈
为了更好的了解您使用 UGSVSDK 所遇到的问题，方便快速有效定位解决  UGSVSDK 问题，希望您按如下反馈指引反馈issue，方便我们尽快解决您的问题  
[UGSVSDK issue反馈指引](https://github.com/tencentyun/UGSVSDK/blob/master/UGSVSDK%20issue有效反馈模板.md)

## Demo 体验地址

<table style="text-align:center;vertical-align:middle;">
  <tr>
    <th style="text-align:center"><b>iOS 版</b></th>
    <th style="text-align:center"><b>Android 版</b></th>
  </tr>
  <tr>
    <td style="text-align:center"><img src="https://main.qcloudimg.com/raw/eac6fe7646ebfc7aa5a0d2e461cd5c37.png" /></td>
    <td style="text-align:center"><img src="https://main.qcloudimg.com/raw/cd69c6227c8ce974f484f52435b76674.png" /></td>
  </tr>
</table>


<div align="left">
<img src="https://main.qcloudimg.com/raw/1e90b3e4c4eda655c4994bd5da293c97.png" height="391" width="220" >
<img src="https://main.qcloudimg.com/raw/6d2996c86edf6a796b681580f3c1fb05.png" height="391" width="220" >
<img src="https://main.qcloudimg.com/raw/a06caa5a974ff7b129255710840148e1.png" height="391" width="220" >
</div>

<div align="left">
<img src="https://main.qcloudimg.com/raw/b7ada99f174e21e09e7b9c78e96c1858.png" height="391" width="220" >
<img src="https://main.qcloudimg.com/raw/dc697e4f7074e6e5477dab0b1746ea87.png" height="391" width="220" >
<img src="https://main.qcloudimg.com/raw/db67663711a7680886a86534e4937e54.png" height="391" width="220" >
</div>













